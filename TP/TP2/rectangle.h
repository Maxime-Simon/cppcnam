#ifndef RECTANGLE_H
#define RECTANGLE_H

#include <stdlib.h>
#include "point.h"

class Rectangle{
public :
    //constructeur
    Rectangle(int width, int height, Point topLeft);

    //getters & setters
    int getWidth() const{
        return width;
    }
    int getHeight() const{
        return height;
    }
    Point getTopLeftPoint() const{
        return topLeft;
    }

    void setWidth(int _width){
        width = _width;
    }
    void setHeight(int _height){
        height = _height;
    }
    void setTopLeftPoint(Point _point){
        topLeft = _point;
    }

    //method
    float getPerimeter() const;
    float getArea() const;
    bool hasBiggerPerimeter(Rectangle rect);
    bool hasBiggerArea(Rectangle rect);
    void Afficher() const;

private:
    //attributs
    int width;
    int height;
    Point topLeft;
};

#endif // RECTANGLE_H
