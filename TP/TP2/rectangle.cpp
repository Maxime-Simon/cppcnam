#include "rectangle.h"

Rectangle::Rectangle(int _width, int _height, Point _topLeft):width(_width),height(_height),topLeft(_topLeft){

}

float Rectangle::getPerimeter() const{
    return getHeight()*2 + getWidth()*2;
}

float Rectangle::getArea() const{
    return getHeight()*getWidth();
}

bool Rectangle :: hasBiggerPerimeter (Rectangle rect){
    if(rect.getPerimeter() < getPerimeter()){
        return true;
    }
    else{
        return false;
    }
}

bool Rectangle :: hasBiggerArea (Rectangle rect){
    if(rect.getArea() < getArea()){
        return true;
    }
    else{
        return false;
    }
}

void Rectangle::Afficher() const{
    const Point topLeft = getTopLeftPoint();
    std::cout << "Largeur du rectangle : " << getWidth() << "\n"
                 "hauteur du rectangle : " << getHeight() <<"\n"
                 "point du coin en haut a gauche du rectangle : (" << topLeft.x << ", " << topLeft.y << ")\n"
                 "Perimetre du rectangle : " << getPerimeter() << "\n"
                 "Surface du rectangle : " << getArea() << "\n"
                 "La classe Rectangle possede plusieurs methode :\n"
                 "\t - hasBiggerPerimeter(Rectangle rect) permettant de savoir si un rectangle a un perimetre plus grand qu'un autre, \n"
                 "\t - hasBiggerArea(Rectangle rect) permettantt de savoir si un rectangle a une surface plus grande qu'un autre\n";
}
