#ifndef POINT_H
#define POINT_H
#include <math.h>
#include <iostream>
struct Point{
    //constructeur
    Point(float x, float y);

    //atributs
    float x;
    float y;

    //method
    float calculDistance(Point point1);
    void Afficher() const;
};

#endif // POINT_H
