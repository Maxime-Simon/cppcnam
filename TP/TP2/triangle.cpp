#include "triangle.h"

Triangle::Triangle(Point a, Point b, Point c):a(a),b(b),c(c)
{

}
std::array<float,3> Triangle::getLengths() const{
    std::array<float,NB_SIDE> lengths={
           getA().calculDistance(b),
           getB().calculDistance(c),
           getC().calculDistance(a)};
       return lengths;
}

float Triangle::getBase() const{
    std::array<float, NB_SIDE> lengths = this->getLengths();
    float base = lengths[0]>lengths[1] ? lengths[0] : lengths[1];
    return lengths[2]>base ? lengths[2] : base;
}

float Triangle::getPerimeter() const{
    std::array<float, NB_SIDE> lengths = this->getLengths();
    return lengths[0]+lengths[1]+lengths[2];
}

float Triangle::getArea() const{
    std::array<float, NB_SIDE> lengths = this->getLengths();
    float perimeter = this->getPerimeter();
    return sqrt(perimeter*(perimeter-lengths[0])*(perimeter-lengths[1])*(perimeter-lengths[2]));
}

float Triangle::getHigh() const{
    return (this->getBase()*this->getArea())/2;
}

bool Triangle::isIsocel() const{
    std::array<float, NB_SIDE> lengths = this->getLengths();
    return lengths[0]==lengths[1] || lengths[1]==lengths[2] || lengths[2] == lengths[0];
}

bool Triangle::isRectangle() const{
    std::array<float,NB_SIDE> lengths = this->getLengths();
    float ab = powf(lengths[0], 2);
    float bc = powf(lengths[1], 2);;
    float ca = powf(lengths[2], 2);;

    return (ab+bc) == ca || (ab+ca) == bc || (bc+ca == ab);
}

bool Triangle::isEquilateral() const{
    std::array<float,NB_SIDE> lengths=this->getLengths();
    return lengths[0]==lengths[1] && lengths[1]==lengths[2];
}

void Triangle::Afficher() const{
    std::array<float,NB_SIDE> lengths=this->getLengths();
    std::cout << "Les 3 points du triangles sont :\n"
                 "\t - A(" << getA().x <<", "<<getA().y <<")\n"
                 "\t - A(" << getB().x <<", "<<getB().y <<")\n"
                 "\t - A(" << getC().x <<", "<<getC().y <<")\n"
                 "Longeur des cotes du triangle : \n"
                 "\t -AB :" << lengths[0] << "\n"
                 "\t -BC :" << lengths[1] << "\n"
                 "\t -CA :" << lengths[2] << "\n"
                 "Base du triangle : " << getBase() <<"\n"
                 "Hauteur du triangle" << getHigh() <<"\n"
                 "Perimetre du triangle : " << getPerimeter() << "\n"
                 "Surface du triangle : " << getArea() << "\n"
                 "La classe triangle possede plusieurs methode :\n"
                 "\t - isIsocel permettant de savoir si il est isocel \n"
                 "\t - isRectangle permettant de savoir si il est rectangle \n"
                 "\t - isEquilateral permettant de savoir si il est equilateral \n";
}
