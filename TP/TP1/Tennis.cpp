#include "Tennis.h"

int main (int argc, char const *argv[]){
    process(4,2);
    process(2,4);
    process(4,0);
    process(8,7);
    process(7,8);
    process(6,6);
    process(15,13);
    process(13,15);
}

void process (int nbVic1, int nbVic2){
 int score1 = ClassicScore(nbVic1, nbVic2);
 int score2 = ClassicScore(nbVic2, nbVic1);

 if(score1!=-1  && score2!=-1){
     noEqualityResult(score1, score2);
 }
 else{
     EqualityScore(nbVic1, nbVic2);
 }

}

int ClassicScore(int nbVic1, int nbVic2){
    if(nbVic1==0){
        return 0;
    }
    else if(nbVic1==1){
        return 15;
    }
    else if(nbVic1== 2){
        return 30;
    }
    else if(nbVic1==3){
        return 40;
    }
    else if(nbVic1==4 && nbVic2<3){
        return 50;
    }
    else{
        return -1;
    }
}

void EqualityScore(int nbVic1, int nbVic2){
    int score1;
    int score2;    
    if(nbVic1 == nbVic2){
        score1=40;
        score2=40;
    }
    else if(nbVic1-1==nbVic2){
        score1= 45;
        score2= 40;
    }
    else if(nbVic2-1==nbVic1){
        score2= 45;
        score1= 40;
    }
    else if(nbVic1-2 == nbVic2){
        score1=50;
        score2=40;
    }
    else if(nbVic2-2 == nbVic1){
        score1=40;
        score2=50;
    }
    else{
        score1=-3;
        score2=-3;
    }
    EqualityResult(score1, score2);
}

void noEqualityResult(int score1, int score2){
    std::cout << "Score : " << score1 << " - " << score2 << "\n";
}

void EqualityResult(int score1, int score2){
    if(score1>score2 && score1==45){
        std::cout << "Score : " << "adv" << " - " << score2 << "\n";
    }
    else if(score2>score1 && score2==45)
    {
        std::cout << "Score : " << score1 << " - " << "adv"  << "\n";
    }
    else if(score1==score2 || score1>score2 || score2>score1){
        std::cout << "Score : " << score1 << " - " << score2 << "\n";
    }
    else{
        std::cout << "On compte uniquemennt au sein d'un jeu et ne comptons pas les set, les valeur entré sont invalide dans ce cas présent\n" ;
    }
}