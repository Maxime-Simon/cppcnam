#include "game.h"

Game::Game(Grid* _grid, int _choice):grid(_grid),choice(_choice)
{
    gameOngoing();
}

void Game::gameOngoing()
{
    //Dire bonjour aux joueurs
    //Initialisation des joueurs => nom
    grid->init();
    gameStarting();
    grid->displayGrid();

    //Commencer la partie avec un tour par tour et une verification de la victoire à chaque tour
    bool fin=false;
    bool deposeJeton=false;
    while(!fin){
        for(int i=1;i<3 && !fin ;i++){
            while(!deposeJeton){
                deposeJeton=caseChoose(i);
                if(!deposeJeton){
                    std::cout<<"Error : Il faut rentrer un chiffre entre 0 et 2 sur une case vide"<<std::endl;
                }
            }

            grid->displayGrid();
            fin=this->gameEnding();
            deposeJeton=false;
        }
    }

}

void Game::gameStarting() {
    std::cout<<"Bonjour et bienvenue."<<std::endl;

    for(int i=1;i<3;i++){
        std::cout<<"Joueur "<<i<<" : Entre ton prenom "<<std::endl;
        std::string prenom;
        std::cin>>prenom;
        if(i==1)
            j1.setNom(prenom);
        else
            j2.setNom(prenom);
    }
    std::cout<<"C'EST PARTI ! "<<std::endl;
}

bool Game::caseChoose(const int player)
{
    if(static_cast<GridMorpion*>(this->grid) != nullptr && this->choice==1){
        GridMorpion* gridMorpion = static_cast<GridMorpion*>(this->grid);
        std::cout<<"Joueur "<<player<<", entre le numero de la ligne et de la colonne ou tu veux mettre ton jeton :"<<std::endl;
        std::string ligne;
        std::string colonne;
        std::cin>>ligne>>colonne;

        int l=std::stoi(ligne);
        int c=std::stoi(colonne);

        if(chooseVerification(l)&&chooseVerification(c)&&gridMorpion->putToken(l,c,player)){
            return true;
        }
        return false;
    }
    else if(static_cast<GridPower4*>(this->grid) != nullptr && this->choice==2){
        GridPower4* gridPower4 = static_cast<GridPower4*>(this->grid);
        std::cout<<"Joueur "<<player<<", entre le numero de la colonne ou tu veux mettre ton jeton :"<<std::endl;
        std::string colonne;
        std::cin>>colonne;

        int c=std::stoi(colonne);

        if(chooseVerification(c)&&gridPower4->putToken(c,player)){
            return true;
        }
        return false;
    }

}

bool Game::chooseVerification(const int i) const
{
    if(i<0 || i>this->grid->getNB_COLUMN()){
        return false;
    }
    return true;
}

bool Game::gameEnding()
{
    if(static_cast<GridMorpion*>(this->grid) != 0 && this->choice==1){
        GridMorpion* gridMorpion = static_cast<GridMorpion*>(this->grid);
        //si quelqu'un a gagné
        if(gridMorpion->playerVictory(1)){
            congratulations(1);
            return true;
        }
        else if(gridMorpion->playerVictory(2)){
            congratulations(2);
            return true;
        }
        //si la grid est pleine
        else if (grid->isFull()){
            draw();
            return true;
        }
        else{
            return false;
        }
    }
    else if(static_cast<GridPower4*>(this->grid) != 0 && this->choice==2){
        GridPower4* gridPower4 = static_cast<GridPower4*>(this->grid);
        //si quelqu'un a gagné
        if(gridPower4->playerVictory(1)){
            congratulations(1);
            return true;
        }
        else if(gridPower4->playerVictory(2)){
            congratulations(2);
            return true;
        }
        //si la grid est pleine
        else if (grid->isFull()){
            draw();
            return true;
        }
        else{
            return false;
        }
    }

}

void Game::congratulations(const int winner) const
{
    if(winner==1){
        std::cout<<"Nous avons un VAINQUEUR ! Bravo "<<j1.getName()<<" !!!"<<std::endl;
    }
    else{
        std::cout<<"Nous avons un VAINQUEUR ! Bravo "<<j2.getName()<<" !!!"<<std::endl;
    }
}

void Game::draw()
{
    std::cout<<"La grid est rempli...match nul, vous voulez rejouer ? Y/N"<<std::endl;
    std::string saisie;
    std::cin>>saisie;

    if(saisie=="Y"){
        std::cout<<"Allez on joue alors ! "<<std::endl;
        gameOngoing();
    }
    else{
        std::cout<<"Merci d'avoir joué, à la prochaine"<<std::endl;
    }
}

