#ifndef GRIDMORPION_H
#define GRIDMORPION_H


#include <stdbool.h>
#include <array>
#include <iostream>
#include <grid.h>

class GridMorpion:public Grid
{
public:
    GridMorpion();
    bool putToken(int i, int j, int player);
    bool completeDiagonal(int diagonal,int player) const; //j etant le numero de colonne d'un coin du tableau
    bool playerVictory(int player) const;
};

#endif // GRIDMORPION_H
